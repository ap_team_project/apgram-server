package com.APproject.apgram.Server;

import com.APproject.apgram.Server.Clinet.ClientsManager;
import com.APproject.apgram.Server.Data.Database.DatabaseManager;
import com.APproject.apgram.Server.Files.FileManager;
import com.APproject.apgram.Server.Util.IdMaker;
import com.APproject.apgram.Server.Util.LocalIdManager;
import com.APproject.apgram.Server.net.Connection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Main implements Runnable{

    public static void main(String[] args) {
        new Main();
    }
    ServerSocket serverSocket;
    JFrame frame;
    JButton button;
    public Main() {
        frame = new JFrame("APGRAM server");
        frame.setSize(200,300);
        button = new JButton("start");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                start();
            }
        });
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(button,BorderLayout.CENTER);
        frame.add(panel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public void close(){
        ClientsManager.getInstance().close();
        IdMaker.getInstance().close();
        LocalIdManager.getInstance().close();
        DatabaseManager.getInstance().close();
        frame.setTitle("APGRAM server"+ " : closed");
        button.setText("start");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                start();
            }
        });
        try {
            serverSocket.close();
        } catch (IOException e) {}
    }
    public void start(){
        new FileManager().loadIdMaker();
        FileManager.getInstance().loadLocalIdManager();
        new ClientsManager();
        FileManager.getInstance().loadDatabaseManager();
        button.setText("close");
        frame.setTitle("APGRAM server"+ " : started");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                close();
            }
        });
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                serverSocket = new ServerSocket(8889);
                break;
            } catch (IOException e) {
            }
        }
        while (!Thread.interrupted()){
            try {
                Socket socket = serverSocket.accept();
                new Connection(socket);
            } catch (IOException e) {}
        }
    }
}
