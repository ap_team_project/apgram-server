package com.APproject.apgram.Server.Files;

import com.APproject.apgram.Server.Data.Database.DatabaseManager;
import com.APproject.apgram.Server.Util.IdMaker;
import com.APproject.apgram.Server.Util.LocalIdManager;
import com.APproject.apgram.Server.Util.SeasonManager;
import com.APproject.apgram.Server.Util.StringCoderAndDecoder;

import java.io.*;
import java.util.*;

public class FileManager {
    private static FileManager singleton_fileManager;
    public static FileManager getInstance(){
        return singleton_fileManager;
    }

    private static final String messageFilesDir = "messageFiles";
    private static final String iconFilesDir = "icons";
    public static final String seasonManagerDir = "seasonManager";
    private static final int sleepTime = 20000;


    Set<TwoVariableCache> cache = new HashSet<>();

    public File filesDir;

    public FileManager(){
        filesDir = new File(new File(System.getProperty("user.home")),"ApgramServer");
        if(!filesDir.exists()){
            filesDir.mkdir();
        }
        File file = new File(filesDir,messageFilesDir);
        if(!file.exists()){
            file.mkdir();
        }
        file = new File(filesDir,iconFilesDir);
        if(!file.exists()){
            file.mkdir();
        }
        file = new File(filesDir,seasonManagerDir);
        if(!file.exists()){
            file.mkdir();
        }
        singleton_fileManager = this;
    }

    public void loadIdMaker(){
        File file = new File(filesDir,"IdMaker");
        if(!file.exists())
            new IdMaker().start();
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            ((IdMaker)in.readObject()).start();
        } catch (IOException | ClassNotFoundException e) {}

    }
    public void saveIdMaker(){
        File file = new File(filesDir,"IdMaker");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {}
        }
        try {
            ObjectOutputStream out= new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(IdMaker.getInstance());
            out.flush();
            out.close();
        } catch (IOException e) {}
    }
    public void loadLocalIdManager(){
        File file = new File(filesDir,"LocalIdManager");
        if(!file.exists())
            new LocalIdManager().start();
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            ((LocalIdManager)in.readObject()).start();
        } catch (IOException | ClassNotFoundException e) {}
    }
    public void saveLocalIdManager(){
        File file = new File(filesDir,"LocalIdManager");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {}
        }
        try {
            ObjectOutputStream out= new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(LocalIdManager.getInstance());
            out.flush();
            out.close();
        } catch (IOException e) {}
    }
    public void loadDatabaseManager(){
        File file = new File(filesDir, DatabaseManager.class.getName());
        if(!file.exists()) {
            new DatabaseManager();
            return;
        }
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            ((DatabaseManager)in.readObject()).start();
        } catch (IOException | ClassNotFoundException e) {}
    }
    public void saveDatabaseManager(){
        File file = new File(filesDir, DatabaseManager.class.getName());
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                return;
            }
        }
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(DatabaseManager.getInstance());
            out.flush();
            out.close();
        } catch (IOException e) {}

    }

    public void saveFileMessage(String messageID, String chatID,String hexString,long start, boolean isLastSection){
        if(start == 0){
            cache.add(new TwoVariableCache(chatID,messageID));
        }
        File chatDir = new File(new File(filesDir, messageFilesDir),chatID);
        if(!chatDir.exists())
            chatDir.mkdir();
        File file = new File(chatDir,messageID);
        writeToFile(file, hexString, start);
        if(isLastSection){
            cache.remove(new TwoVariableCache(chatID, messageID));
        }
    }
    public void saveIcon(String id, String hexString, long start, boolean isLastSection){
        if(start == 0){
            cache.add(new TwoVariableCache("icon",id));
        }
        File file = new File(new File(filesDir,iconFilesDir),id);
        writeToFile(file, hexString, start);
        if(isLastSection){
            cache.remove(new TwoVariableCache("icon",id));
        }
    }
    private void writeToFile(File file, String hexString, long start) {
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {}
        }
        try {
            RandomAccessFile out = new RandomAccessFile(file, "rw");
            byte[] bytes = StringCoderAndDecoder.decodeHexString(hexString);
            out.seek(start);
            out.write(bytes);
            out.close();
        } catch (Exception e) {}
    }



    public File getMessageFile(String messageID, String chatID){
        TwoVariableCache a = new TwoVariableCache(chatID, messageID);
        while (cache.contains(a)){
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {}
        }
        return new File(new File(new File(filesDir,messageFilesDir),chatID),messageID);
    }
    public File getIconFile(String id){
        TwoVariableCache a = new TwoVariableCache("icon",id);
        while (cache.contains(a)){
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {}
        }
        return new File(new File(filesDir,iconFilesDir),id);
    }

    public SeasonManager getSeasonManager(String id){
        File file = new File(new File(filesDir, seasonManagerDir),id);
        if(!file.exists())return null;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            return (SeasonManager) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
        }
        return null;
    }
    public void saveSeasonManager(SeasonManager seasonManager){
        File file = new File(new File(filesDir,seasonManagerDir),seasonManager.getID());
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {}
        }
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(seasonManager);
        } catch (IOException e) {}
    }
}
