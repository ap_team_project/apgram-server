package com.APproject.apgram.Server.Files;

import java.util.Objects;

public class TwoVariableCache {
    String chatID;
    String messageID;

    public TwoVariableCache(String chatID, String messageID) {
        this.chatID = chatID;
        this.messageID = messageID;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TwoVariableCache that = (TwoVariableCache) o;
        return Objects.equals(chatID, that.chatID) &&
                Objects.equals(messageID, that.messageID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chatID, messageID);
    }
}
