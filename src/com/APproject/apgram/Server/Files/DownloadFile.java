package com.APproject.apgram.Server.Files;

import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DownloadFile implements Serializable {
    String id;
    String chatID;
    int index = 1;
    int count;
    long eachStepSize;
    FileType type;
    Lock lock;

    public DownloadFile(int count, long eachStepSize, String id ,String chatID,FileType type) {
        this.count = count;
        this.eachStepSize = eachStepSize;
        this.type = type;
        this.id = id;
        this.chatID = chatID;
        lock = new ReentrantLock();
    }

    public boolean save(int index , String hexString){
        lock.lock();
        long start = (index-1)*eachStepSize;
        if(type == FileType.ICON){
            FileManager.getInstance().saveIcon(id, hexString,start, count==this.index);
        }
        else{
            FileManager.getInstance().saveFileMessage(id,chatID,hexString,start,count==this.index);
        }
        if(this.index  == count) {
            lock.unlock();
            return true;
        }
        this.index++;
        lock.unlock();
        return false;
    }

}

enum FileType{
    MESSAGE_FILE,
    ICON
}
