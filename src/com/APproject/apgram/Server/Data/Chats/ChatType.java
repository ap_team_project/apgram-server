package com.APproject.apgram.Server.Data.Chats;

public enum ChatType {
    GROUP,
    PRIVATE,
    CHANNEL
}
