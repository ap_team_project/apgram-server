package com.APproject.apgram.Server.Data.Chats;

public enum Access {
    OWNER,
    ADMIN,
    NORMAL
}
