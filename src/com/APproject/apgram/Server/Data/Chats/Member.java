package com.APproject.apgram.Server.Data.Chats;


import com.APproject.apgram.Server.Data.User.User;

public class Member {
    String userID;
    Access access;

    public Member(String userID, Access access) {
        this.userID = userID;
        this.access = access;
    }

    public String getUserID() {
        return userID;
    }

    public Access getAccess() {
        return access;
    }
}
