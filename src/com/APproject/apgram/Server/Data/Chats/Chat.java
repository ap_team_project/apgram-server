package com.APproject.apgram.Server.Data.Chats;


import com.APproject.apgram.Server.Data.HaveSeasonAndIcon;
import com.APproject.apgram.Server.Data.Messages.Message;



public class Chat extends HaveSeasonAndIcon{
    String id;
    ChatType type;
    String chatName;
    String lastMessageID;


    public Chat(String id, ChatType type,boolean haveIcon) {
        super(haveIcon, 1,(haveIcon)?1:0);
        this.id = id;
        this.type = type;

    }

    public Chat(String id, ChatType type, String chatName, boolean haveIcon) {
        super(haveIcon, 1,(haveIcon)?1:0);
        this.id = id;
        this.type = type;
        this.chatName = chatName;
    }

    public Chat(String id, ChatType type,boolean haveIcon,int seasonID,int iconSeasonID) {
        super(haveIcon, seasonID,iconSeasonID);
        this.id = id;
        this.type = type;

    }

    public Chat(String id, ChatType type, String chatName, boolean haveIcon,int seasonID,int iconSeasonID) {
        super(haveIcon, seasonID,iconSeasonID);
        this.id = id;
        this.type = type;
        this.chatName = chatName;
    }

    public String getID(){
        return id;
    }
    public ChatType getChatType(){
        return type;
    }
    public String getChatName(){
        return chatName;
    }




    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    @Override
    public boolean equals(Object obj) {
        if ((obj instanceof Chat)) {
            Chat c = (Chat) obj;
            return id.equals(c.getID());
        }
        else if(obj instanceof String){
            return id.equals(obj);
        }
        return false;
    }
}
