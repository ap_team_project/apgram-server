package com.APproject.apgram.Server.Data.User;

import com.APproject.apgram.Server.Data.HaveSeasonAndIcon;

import java.util.Date;

public class User extends HaveSeasonAndIcon {
    String userName;
    String ID;
    boolean isOnline;
    Date lastSeen;


    public User(String userName, String ID, boolean isOnline, Date lastSeen, boolean haveIcon,int seasonID, int iconSeasonID) {
        super(haveIcon, seasonID,iconSeasonID);
        this.userName = userName;
        this.ID = ID;
        this.isOnline = isOnline;
        this.lastSeen = lastSeen;
    }

    public Date getLastSeen(){
        return lastSeen;
    }

    public String getID(){
        return ID;
    }

    public String getUserName(){
        return userName;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public User setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public User setOnline(boolean online) {
        isOnline = online;
        return this;
    }

    public User setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
        return this;
    }
}
