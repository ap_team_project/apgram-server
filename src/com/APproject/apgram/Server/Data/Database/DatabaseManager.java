package com.APproject.apgram.Server.Data.Database;

import com.APproject.apgram.Server.Data.Chats.Access;
import com.APproject.apgram.Server.Data.Chats.Chat;
import com.APproject.apgram.Server.Data.Chats.Member;
import com.APproject.apgram.Server.Data.Messages.Message;
import com.APproject.apgram.Server.Data.User.User;
import com.APproject.apgram.Server.Files.FileManager;
import com.APproject.apgram.Server.Files.TwoVariableCache;

import java.io.Closeable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DatabaseManager implements Serializable, Closeable {
    static DatabaseManager singleDatabaseManager;

    public static DatabaseManager getInstance() {
        return singleDatabaseManager;
    }



    HashMap<String, User> users;
    HashMap<String, ArrayList<String>> userChats;
    HashMap<String, Chat> chats;
    HashMap<String, ArrayList<Member>> members;
    HashMap<String, ArrayList<Message>> messages;
    HashMap<TwoVariableCache,String> userPassword;

    public DatabaseManager(){
        users = new HashMap<>();
        userChats = new HashMap<>();
        chats = new HashMap<>();
        members = new HashMap<>();
        messages = new HashMap<>();
        userPassword = new HashMap<>();
        start();
    }

    public void start() {
        singleDatabaseManager = this;
    }

    public User getUser(String userID){
        return users.getOrDefault(userID, null);
    }
    public void setSignOutTime(String userID, Date date){
        if(!users.containsKey(userID))
            return;
        users.put(userID,users.get(userID).setLastSeen(date));
    }
    public void updateUser(User user){
        if(users.containsKey(user.getID())){
            return;
        }
        users.put(user.getID(),
                users.get(user.getID()).setUserName(user.getUserName()).setOnline(user.isOnline()));

    }
    public void addUserIconSeasonID(String userID){
        if(!users.containsKey(userID)){
            return;
        }
        User user = users.get(userID);
        user.addIconSeasonID();
        users.put(userID, user);
    }


    public ArrayList<Chat> getUserChats(String userID){
        if(!users.containsKey(userID))
            return new ArrayList<>();
        ArrayList<String> chatsIDs = userChats.get(userID);
        ArrayList<Chat> result = new ArrayList<>();
        for(String chatID:chatsIDs){
           Chat chat = getChat(chatID);
           if(chat != null)
               result.add(chat);
        }
        return result;
    }
    public Chat getChat(String chatID){
        return chats.getOrDefault(chatID, null);
    }
    public void addChat(Chat chat){
        if(chats.containsKey(chat.getID()))
            return;
        chats.put(chat.getID(),chat);
    }
    public void updateChat(Chat chat){
        if(chats.containsValue(chat.getID())){
            Chat c = chats.get(chat.getID());
            c.setChatName(chat.getChatName());
            c.addSeasonID();
            chats.put(chat.getID(),c);
        }
    }
    public void addChatIconSeasonID(String chatID){
        if(chats.containsValue(chatID)){
            Chat c = chats.get(chatID);
            c.addIconSeasonID();
            chats.put(chatID,c);
        }
    }
    public void removeChat(String chatID){
        chats.remove(chatID);
    }

    public ArrayList<Member> getChatMembers(String chatID){
        if(chatID.charAt(0) != 'p'){
            return members.getOrDefault(chatID, new ArrayList<>());
        }
        ArrayList<Member> result = new ArrayList<>();
        String firstId = chatID.substring(1);
        int sharpIndex = firstId.indexOf("#");
        String secondId = firstId.substring(sharpIndex+1);
        firstId = firstId.substring(0,sharpIndex);
        result.add(new Member(firstId,Access.NORMAL));
        result.add(new Member(secondId,Access.NORMAL));
        return result;
    }
    public void addMember(String chatID, Member member){
        ArrayList<Member> arrayList = members.getOrDefault(chatID, new ArrayList<>());
        if(!users.containsKey(member.getUserID())){
            return;
        }
        ArrayList<String> userChat = userChats.getOrDefault(member.getUserID(), new ArrayList<>());
        userChat.add(chatID);
        userChats.put(member.getUserID(),userChat);
        arrayList.add(member);
        members.put(chatID,arrayList);
        Chat c = chats.get(chatID);
        c.addSeasonID();
        chats.put(chatID,c);
    }

    public void addMessage(String chatID, Message message) {
        if(!chats.containsKey(chatID)) {
            if(chatID.charAt(0) != 'p')
                return;
        }
        if(chatID.charAt(0) == 'c')
            message.setSenderId(null);
        ArrayList<Message> arrayList = messages.getOrDefault(chatID, new ArrayList<>());
        arrayList.add(message);
        messages.put(chatID, arrayList);
    }

     public ArrayList<Message> getMessage(String chatID, String messageID){
        if(!messages.containsKey(chatID)){
            return new ArrayList<>();
        }
        ArrayList<Message> arrayList = messages.get(chatID);
         int intID = Integer.parseInt(messageID.substring(1));
        if(messageID == null || intID == 0){
            return arrayList;
        }
        ArrayList<Message> result = new ArrayList<>();
        boolean canPut = false;
        for(Message message:arrayList){
            if(!canPut){
                if(message.getId().equals(messageID)){
                    canPut = true;
                    continue;
                }
            }
            else {
                result.add(message);
            }
        }
        return result;
    }


    public String getUserIdFromUsername(String username){
        for(Map.Entry<String,User> entry:users.entrySet()){
            if(entry.getValue().getUserName().equals(username))
                return entry.getKey();
        }
        return null;
    }

    public String getUserID(String username, String password){
        TwoVariableCache cache = new TwoVariableCache(username,password);
        return userPassword.getOrDefault(cache,null);
    }
    public boolean isUsernameExist(String userID){
        return getUserIdFromUsername(userID)==null?false:true;
    }
    public void createUser(String username, String password, String userID){
        userPassword.put(new TwoVariableCache(userID,password),userID);
        User user = new User(username, userID,false,new Date(),false,1,0);
        users.put(userID,user);
    }

    @Override
    public void close(){
        FileManager.getInstance().saveDatabaseManager();
    }
}
