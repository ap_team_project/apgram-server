package com.APproject.apgram.Server.Data.Messages;

public enum MessageType {
    TEXT,
    FILE,
    IMAGE,
    VIDEO,
    MUSIC
}
