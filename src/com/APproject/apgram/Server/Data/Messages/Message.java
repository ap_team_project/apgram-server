package com.APproject.apgram.Server.Data.Messages;

import java.util.Date;

public class Message {
    String id = null;
    String textContent = null;
    String senderId = null;
    MessageType type;
    Date sendDate;

    public Message(String id, String textContent, String senderId, MessageType type, Date sendDate) {
        this.id = id;
        this.textContent = textContent;
        this.senderId = senderId;
        this.type = type;
        this.sendDate = sendDate;
    }

    public Message(String id, String senderId, MessageType type, Date sendDate) {
        this.id = id;
        this.senderId = senderId;
        this.type = type;
        this.sendDate = sendDate;
    }

    public String getContent(){
       return textContent;
   }
    public String getId() {
        return id;
    }
    public String getSender(){
        return senderId;
    }
    public MessageType getType(){
        return type;
    }
    public String getDate(){
        return sendDate.toString();
    }

    public Message setSenderId(String senderId) {
        this.senderId = senderId;
        return this;
    }
}

