package com.APproject.apgram.Server.Data;

public class HaveSeasonAndIcon {
    boolean haveIcon;
    int iconSeasonID;
    int seasonID;

    public HaveSeasonAndIcon(boolean haveIcon,int seasonID ,int iconSeasonID) {
        this.haveIcon = haveIcon;
        this.seasonID = seasonID;
        this.iconSeasonID = iconSeasonID;
    }

    public boolean isHaveIcon() {
        return haveIcon;
    }

    public int getSeasonID() {
        return seasonID;
    }

    public int getIconSeasonID() {
        return iconSeasonID;
    }

    public HaveSeasonAndIcon setHaveIcon(boolean haveIcon) {
        this.haveIcon = haveIcon;
        return this;
    }

    public HaveSeasonAndIcon setIconSeasonID(int iconSeasonID) {
        this.iconSeasonID = iconSeasonID;
        return this;
    }

    public HaveSeasonAndIcon setSeasonID(int seasonID) {
        this.seasonID = seasonID;
        return this;
    }

    public HaveSeasonAndIcon addSeasonID() {
        seasonID++;
        return this;
    }

    public HaveSeasonAndIcon addIconSeasonID() {
        iconSeasonID++;
        return this;
    }
}
