package com.APproject.apgram.Server.Clinet;

import com.APproject.apgram.Server.Data.Chats.Member;
import com.APproject.apgram.Server.Data.Database.DatabaseManager;
import com.APproject.apgram.Server.Util.IdMaker;
import com.APproject.apgram.Server.net.Connection;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class ClientsManager implements Closeable {
    private static ClientsManager singleClientsManager;

    public static ClientsManager getInstance() {
        return singleClientsManager;
    }

    HashMap<String , Client> clients = new HashMap<>();


    public boolean signIn(String userName, String password, Connection connection){
        String id = DatabaseManager.getInstance().getUserID(userName,password);
        if(id == null)
            return false;
        Client client = new Client(id,connection);
        clients.put(id,client);
        return true;
    }
    public String createAccount(String userName, String password,Connection connection){
        if(DatabaseManager.getInstance().isUsernameExist(userName))
            return null;
        String newID = IdMaker.getInstance().getUserID();
        DatabaseManager.getInstance().createUser(userName, password, newID);
        Client client = new Client(newID, connection);
        return newID;
    }

    public void userSignOut(String userID){
        clients.get(userID).close();
        clients.remove(userID);
        Date date = new Date();
        DatabaseManager.getInstance().setSignOutTime(userID, date);
    }

    public Client getClient(String userID){
        return clients.getOrDefault(userID,null);
    }
    public void haveNewMessage(String chatID){
        ArrayList<Member> members = DatabaseManager.getInstance().getChatMembers(chatID);
        for(Member member:members){
            if(clients.containsKey(member.getUserID())){
                clients.get(member.getUserID()).checkHaveMessage(chatID);
            }
        }
    }
    public void addChat(String chatID){
        ArrayList<Member> members = DatabaseManager.getInstance().getChatMembers(chatID);
        for(Member member: members){
            Client client = getClient(member.getUserID());
            if(client != null){
                client.checkChatUpdate(chatID);
            }
        }
    }
    public void addMemberToChat(String chatID, String userID){
        Client client = getClient(userID);
        if(client == null)
            return;
        client.checkChatUpdate(chatID);
    }


    @Override
    public void close(){
        for(Client client:clients.values()){
            client.close();
        }
    }
}
