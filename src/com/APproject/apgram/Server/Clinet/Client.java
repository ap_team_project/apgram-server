package com.APproject.apgram.Server.Clinet;

import com.APproject.apgram.Server.Data.Chats.Access;
import com.APproject.apgram.Server.Data.Chats.Chat;
import com.APproject.apgram.Server.Data.Chats.ChatType;
import com.APproject.apgram.Server.Data.Chats.Member;
import com.APproject.apgram.Server.Data.Database.DatabaseManager;
import com.APproject.apgram.Server.Data.Messages.Message;
import com.APproject.apgram.Server.Data.Messages.MessageType;
import com.APproject.apgram.Server.Data.User.User;
import com.APproject.apgram.Server.Files.FileManager;
import com.APproject.apgram.Server.Util.SeasonManager;
import com.APproject.apgram.Server.net.Connection;
import com.APproject.apgram.Server.net.ServerRequestHandlling.AddContactServerRequest;
import com.APproject.apgram.Server.net.ServerRequestHandlling.SendMessageToClient;
import com.APproject.apgram.Server.net.ServerRequestHandlling.SendFileToClient;
import com.APproject.apgram.Server.net.ServerRequestHandlling.ChangeProfileIconServerRequest;
import com.APproject.apgram.Server.net.ServerRequestHandlling.UpdateUserInformationServerRequest;
import com.APproject.apgram.Server.net.ServerRequestHandlling.UpdateChatServerRequest;
import com.APproject.apgram.Server.net.ServerRequestHandlling.ChangeGroupIconServerRequest;
import com.APproject.apgram.Server.net.ServerRequestHandlling.ChangeChannelIconServerRequest;
import com.APproject.apgram.Server.net.ServerRequestHandlling.AddGroupServerRequest;
import com.APproject.apgram.Server.net.ServerRequestHandlling.AddChannelServerRequest;

import javax.xml.crypto.Data;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Client implements Closeable,Runnable {
    String id;
    Connection connection;
    SeasonManager seasonManager;
    Lock lock;

    public Client(String id, Connection connection) {
        this.id = id;
        this.connection = connection;
        this.connection.setID(id);
        seasonManager = FileManager.getInstance().getSeasonManager(id);
        connection.setID(id);
        lock = new ReentrantLock();
        new Thread(this).start();
    }

    @Override
    public void run() {
        lock.lock();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {}
        ArrayList<Chat> chats = DatabaseManager.getInstance().getUserChats(id);
        for(Chat c: chats){
            checkHaveMessage(c.getID());
        }
        for(Chat c:chats){
            checkChatUpdate(c);
        }
        lock.unlock();
    }

    public Connection getConnection() {
        return connection;
    }

    public void checkHaveMessage(String chatID){
        ArrayList<Message> messages = DatabaseManager.getInstance().getMessage(chatID, seasonManager.getMessageSeason(chatID));
        if(messages == null)
            return;
        for(Message message:messages){
            if(message.getType() == MessageType.TEXT){
                new SendMessageToClient(chatID, message.getId(), message.getContent(),id);
            }
            else {
                String type = "";
                if(message.getType()==MessageType.FILE)
                    type="FILE";
                else if(message.getType()==MessageType.IMAGE)
                    type="IMAGE";
                else if(message.getType()==MessageType.MUSIC)
                    type="MUSIC";
                else if(message.getType()==MessageType.VIDEO)
                    type="VIDEO";
                String fileAdress = FileManager.getInstance().getMessageFile(message.getId(),chatID).getAbsolutePath();
                new SendFileToClient(chatID,message.getId(),type,fileAdress,id);
            }
            seasonManager.setMessageSeason(chatID,message.getId());
        }

    }
    public void checkUserUpdate(String userID){
        User user = DatabaseManager.getInstance().getUser(userID);
        if(user.getSeasonID() > seasonManager.getSeason(userID)){
            new UpdateUserInformationServerRequest(user.getUserName(),userID,user.getLastSeen(),id);
        }
        if(user.getIconSeasonID() > seasonManager.getIconSeason(userID)){
            new ChangeProfileIconServerRequest(userID, FileManager.getInstance().getIconFile(userID).getAbsolutePath(), id);
        }
    }
    public void checkChatUpdate(String chatID){
        Chat chat = DatabaseManager.getInstance().getChat(chatID);
        if(chat == null)
            return;
        checkChatUpdate(chat);
    }
    private void checkChatUpdate(Chat chat){
        if(seasonManager.getSeason(chat.getID())==0){
            if(chat.getChatType() == ChatType.GROUP){
                ArrayList<Member>members = DatabaseManager.getInstance().getChatMembers(chat.getID());
                new AddGroupServerRequest(chat.getID(),chat.getChatName(),members,id);
            }
            else if(chat.getChatType() == ChatType.CHANNEL){
                ArrayList<Member> members = DatabaseManager.getInstance().getChatMembers(chat.getID());
                boolean sendMembers=false;
                for(Member member:members){
                    if(member.getUserID().equals(id)){
                        if(member.getAccess()!= Access.NORMAL){
                            sendMembers=true;
                            break;
                        }
                    }
                }
                if(!sendMembers)
                    members = new ArrayList<>();
                new AddChannelServerRequest(chat.getID(),chat.getChatName(),members,id);
            }
        }
        if(chat.getSeasonID() > seasonManager.getSeason(chat.getID())){
            ArrayList<Member> members = DatabaseManager.getInstance().getChatMembers(chat.getID());
            boolean sendMemeber = true;
            if(chat.getChatType()==ChatType.CHANNEL) {
                for (Member member : members) {
                    if (member.getUserID().equals(id)) {
                        if (member.getAccess() == Access.NORMAL)
                            sendMemeber = false;
                    }
                }
                if(!sendMemeber)
                    members = new ArrayList<>();
            }
            new UpdateChatServerRequest(chat.getID(),chat.getChatName(),chat.getChatType(),members,id);
            seasonManager.setSeason(chat.getID(),chat.getSeasonID());
        }
        if(chat.getIconSeasonID() > seasonManager.getIconSeason(chat.getID())){
            if(chat.getChatType()==ChatType.GROUP)
                new ChangeGroupIconServerRequest(chat.getID(),FileManager.getInstance().getIconFile(chat.getID()).getAbsolutePath(),id);
            else if(chat.getChatType() == ChatType.CHANNEL)
                new ChangeChannelIconServerRequest(chat.getID(),FileManager.getInstance().getIconFile(chat.getID()).getAbsolutePath(),id);
            seasonManager.setIconSeason(chat.getID(),chat.getIconSeasonID());
        }
    }


    public void addContact(String userID){
        User user = DatabaseManager.getInstance().getUser(userID);
        new AddContactServerRequest(user.getUserName(),userID,user.getLastSeen(),id);
    }



    @Override
    public void close(){
        seasonManager.close();
        try{
            connection.close();
        } catch (IOException e) {}
    }
}
