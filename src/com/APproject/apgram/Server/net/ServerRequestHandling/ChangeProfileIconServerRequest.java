package com.APproject.apgram.Server.net.ServerRequestHandlling;


import com.company.Clinet.ClientsManager;
import com.company.File.UploadFile;
import com.company.net.Connection;
import com.google.gson.Gson;

import java.io.FileNotFoundException;

public class ChangeProfileIconServerRequest extends ServerRequests implements Runnable {
    private String serverRequestTYPE;
    private String userID;
    private int index;
    public static final long eachStepCount = 10000;
    private int count;
    private  String hexString;
    private String fileAddress;
    private String destinationID;

    public ChangeProfileIconServerRequest(String userID , String fileAddresst , String destinationID) {
        this.serverRequestTYPE = "ChangeProfileIconServerRequest";
        this.userID = userID;
        this.fileAddress = fileAddresst;
        this.destinationID = destinationID;
    }

    @Override
    public void run() {
        if (ClientsManager.getInstance().getClient(userID) != null) {
            UploadFile uploadFile = null;
            try {
                uploadFile = new UploadFile(fileAddress);
                this.count = uploadFile.getCount();
            } catch (FileNotFoundException e) {
                System.out.println(e);
            }
            for (int index = 1; index <= this.count; index++) {
                this.index = index;
                this.hexString = uploadFile.next();
                String GSONFILE = new Gson().toJson(this);
                Connection connection = ClientsManager.getInstance().getClient(userID).getConnection();
                connection.sendJSON(GSONFILE);
            }
        }
    }
}
