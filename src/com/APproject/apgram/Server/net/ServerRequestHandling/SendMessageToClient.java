package com.APproject.apgram.Server.net.ServerRequestHandlling;

import com.company.Clinet.Client;
import com.company.Clinet.ClientsManager;
import com.company.net.Connection;
import com.google.gson.Gson;

public class SendMessageToClient extends com.APproject.apgram.Server.net.ServerRequestHandlling.ServerRequests implements Runnable{
    private String serverRequestTYPE;
    private String chatID;
    private String messageID;
    private String text;
    private String destinationID;

    public SendMessageToClient(String chatID , String messageID , String text , String destinationID) {
        this.serverRequestTYPE = "SendMessageToClient";
        this.chatID = chatID;
        this.messageID = messageID;
        this.text = text;
        this.destinationID = destinationID;
        new Thread().start();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getChatID() {
        return chatID;
    }

    public String getRequestTYPE() {
        return serverRequestTYPE;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        Client client = ClientsManager.getInstance().getClient(destinationID);
        if (ClientsManager.getInstance().getClient(destinationID) != null){
            Connection connection = client.getConnection();
            connection.sendJSON(GSONFILE);
        }
    }
}
