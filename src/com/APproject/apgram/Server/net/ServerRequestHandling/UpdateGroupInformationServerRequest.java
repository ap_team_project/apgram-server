package com.APproject.apgram.Server.net.ServerRequestHandlling;

import com.google.gson.Gson;

public class UpdateGroupInformationServerRequest extends ServerRequests implements Runnable{
    private String serverRequestTYPE;
    private String groupID;

    public UpdateGroupInformationServerRequest(String channelID) {
        this.serverRequestTYPE = "UpdateGroupInformationServerRequest";
        this.groupID = channelID;
        new Thread().start();
    }

    public String getGroupID() {
        return groupID;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        //todo connection
    }
}
