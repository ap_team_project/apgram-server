package com.APproject.apgram.Server.net.ServerRequestHandlling;

import com.google.gson.Gson;

public class RenameGroupServerRequest extends ServerRequests implements Runnable{
    private String serverRequestTYPE;
    private String groupID;
    private String groupNewNAME;

    public RenameGroupServerRequest(String channelID, String groupNewNAME) {
        this.serverRequestTYPE = "RenameGroupServerRequest";
        this.groupID = channelID;
        this.groupNewNAME = groupNewNAME;
        new Thread().start();
    }

    public String getRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getGroupID() {
        return groupID;
    }

    public String getGroupNewNAME() {
        return groupNewNAME;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        //todo connection
    }
}
