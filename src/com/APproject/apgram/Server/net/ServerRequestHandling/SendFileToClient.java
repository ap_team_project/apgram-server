package com.APproject.apgram.Server.net.ServerRequestHandlling;

import com.company.Clinet.ClientsManager;
import com.company.File.UploadFile;
import com.company.net.Connection;
import com.google.gson.Gson;

import java.io.FileNotFoundException;

public class SendFileToClient extends ServerRequests implements Runnable {
    private String serverRequestTYPE;
    private String chatID;
    private String messageID;
    private String fileTYPE;
    private int index;
    public static final long eachStepCount = 10000;
    private int count;
    private  String hexString;
    private String fileAddress;
    private String destinationID;

    public SendFileToClient(String chatID ,String messageID,String fileTYPE,String fileAddress , String destinationID) {
        this.serverRequestTYPE = "SendFileToClient";
        this.chatID = chatID;
        this.messageID = messageID;
        this.fileTYPE = fileTYPE;
        this.fileAddress = fileAddress;
        this.destinationID = destinationID;
        new Thread().start();
    }

    @Override
    public void run() {
        if (ClientsManager.getInstance().getClient(destinationID) != null) {
            UploadFile uploadFile = null;
            try {
                uploadFile = new UploadFile(fileAddress);
                this.count = uploadFile.getCount();
            } catch (FileNotFoundException e) {
                System.out.println(e);
            }
            for (int index = 1; index <= this.count; index++) {
                this.index = index;
                this.hexString = uploadFile.next();
                String GSONFILE = new Gson().toJson(this);
                Connection connection = ClientsManager.getInstance().getClient(destinationID).getConnection();
                connection.sendJSON(GSONFILE);
            }
        }
    }
}
