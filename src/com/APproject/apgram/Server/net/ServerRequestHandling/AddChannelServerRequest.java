package com.APproject.apgram.Server.net.ServerRequestHandlling;


import com.company.Clinet.Client;
import com.company.Clinet.ClientsManager;
import com.company.Date.Database.DatabaseManager;
import com.company.net.Connection;
import com.google.gson.Gson;
import com.google.gson.internal.$Gson$Preconditions;

import java.util.ArrayList;

public class AddChannelServerRequest extends ServerRequests implements Runnable {
    private String serverRequestTYPE;
    private String chatID;
    private String chatNAME;
    private ArrayList<String> membersUSERNAME;
    private String destinationID;

    public AddChannelServerRequest(String chatID , String chatNAME ,ArrayList<String> membersUSERNAME , String destinationID ) {
        this.serverRequestTYPE = "AddChannelServerRequest";
        this.chatID = chatID;
        this.chatNAME = chatNAME;
        this.membersUSERNAME = new ArrayList<>(membersUSERNAME);
        this.destinationID = destinationID;
        new Thread().start();
    }
    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        Client client = ClientsManager.getInstance().getClient(destinationID);
        if (ClientsManager.getInstance().getClient(destinationID) != null){
            Connection connection = client.getConnection();
            connection.sendJSON(GSONFILE);
        }
    }
}
