package com.APproject.apgram.Server.net.ServerRequestHandlling;

import com.company.Clinet.Client;
import com.company.Clinet.ClientsManager;
import com.company.Date.Database.DatabaseManager;
import com.company.Date.User.User;
import com.company.net.Connection;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddContactServerRequest extends ServerRequests implements Runnable {
    private String serverRequestTYPE;
    private String userName;
    private String userID;
    private String lastSeen;
    private String destinationID;

    public AddContactServerRequest(String userName, String userID, Date lastSeen, String destinationID) {
        this.serverRequestTYPE = "AddContactServerRequest";
        this.userName = userName;
        this.userID = userID;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        this.lastSeen = dateFormat.format(lastSeen);
        this.destinationID = destinationID;
        new Thread().start();
    }

    public String getRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getUserName() {
        return userName;
    }

    public String getDestinationID() {
        return destinationID;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public String getUserID() {
        return userID;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        String ID = DatabaseManager.getInstance().getUserIdFromUsername(userName);
        Client client = ClientsManager.getInstance().getClient(ID);
        if (ClientsManager.getInstance().getClient(ID) != null){
            Connection connection = client.getConnection();
            connection.sendJSON(GSONFILE);
        }
    }
}


