package com.APproject.apgram.Server.net.ServerRequestHandlling;

import com.google.gson.Gson;

public class RenameChannelServerRequest extends ServerRequests implements Runnable{
    private String serverRequestTYPE;
    private String channelID;
    private String channelNewNAME;

    public RenameChannelServerRequest(String channelID, String channelNewNAME) {
        this.serverRequestTYPE = "RenameChannelServerRequest";
        this.channelID = channelID;
        this.channelNewNAME = channelNewNAME;
        new Thread().start();
    }

    public String getRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getChannelID() {
        return channelID;
    }

    public String getChannelNewNAME() {
        return channelNewNAME;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        //todo connection
    }
}
