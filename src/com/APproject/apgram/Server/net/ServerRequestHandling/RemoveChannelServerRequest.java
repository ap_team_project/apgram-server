package com.APproject.apgram.Server.net.ServerRequestHandlling;

import com.google.gson.Gson;

public class RemoveChannelServerRequest extends ServerRequests implements Runnable{
    private String serverRequestTYPE;
    private String channelID;

    public RemoveChannelServerRequest(String channelID) {
        this.serverRequestTYPE = "RemoveChannelServerRequest";
        this.channelID = channelID;
    }

    public String getRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getChannelID() {
        return channelID;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        //todo connection
    }
}
