package com.APproject.apgram.Server.net.ServerRequestHandlling;

import com.company.Clinet.Client;
import com.company.Clinet.ClientsManager;
import com.company.Date.Chat.ChatType;
import com.company.Date.Chat.Member;
import com.company.net.Connection;
import com.google.gson.Gson;

import java.util.ArrayList;

public class UpdateChatServerRequest extends ServerRequests implements Runnable {
    private String serverRequestTYPE;
    private String chatID;
    private String chatNAME;
    private String chatTYPE;
    private ArrayList<String> membersUSERNAME;
    private String destinationID;

    public UpdateChatServerRequest(String chatID , String chatNAME , ChatType chatTYPE , ArrayList<String> membersUSERNAME,String destinationID) {
        this.serverRequestTYPE = "UpdateChatServerRequest";
        this.chatID = chatID ;
        this.chatNAME = chatNAME;
        if(chatTYPE == ChatType.CHANNEL){
            this.chatTYPE = "CHANNEL";
        }
        else if (chatTYPE == ChatType.GROUP){
            this.chatTYPE = "GROUP";
        }
        else {
            this.chatTYPE = "PRIVATE";
        }
        this.membersUSERNAME = new ArrayList<>(membersUSERNAME);
        this.destinationID = destinationID;
        new Thread().start();
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        for (int i = 0 ; i < membersUSERNAME.size() ; i++){
            Client client = ClientsManager.getInstance().getClient(membersUSERNAME.get(i));
            if (ClientsManager.getInstance().getClient(membersUSERNAME.get(i)) != null) {
                Connection connection = client.getConnection();
                connection.sendJSON(GSONFILE);
            }
        }
    }
}

