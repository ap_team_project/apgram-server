package com.APproject.apgram.Server.net.ServerRequestHandlling;

import com.google.gson.Gson;

public class UpdateChannelInformationServerRequest extends ServerRequests implements Runnable{
    private String serverRequestTYPE;
    private String channelID;

    public UpdateChannelInformationServerRequest(String channelID) {
        this.serverRequestTYPE = "UpdateChannelInformationServerRequest";
        this.channelID = channelID;
        new Thread().start();
    }

    public String getChannelID() {
        return channelID;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        //todo connection
    }
}
