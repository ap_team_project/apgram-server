package com.APproject.apgram.Server.net.ServerRequestHandlling;

import com.company.Clinet.Client;
import com.company.Clinet.ClientsManager;
import com.company.Date.User.User;
import com.company.net.Connection;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UpdateUserInformationServerRequest extends ServerRequests implements Runnable{
    private String serverRequestTYPE;
    private String userName;
    private String userID;
    private String lastSeen;
    private String destinationID;

    public UpdateUserInformationServerRequest(String userName, String userID , Date lastSeen , String destinationID) {
        this.serverRequestTYPE = "UpdateUserInformationServerRequest";
        this.userName = userName;
        this.userID = userID;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        this.lastSeen = dateFormat.format(lastSeen);
        this.destinationID = destinationID;
        new Thread().start();
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        Client client = ClientsManager.getInstance().getClient(userID);
        if (ClientsManager.getInstance().getClient(userID) != null){
            Connection connection = client.getConnection();
            connection.sendJSON(GSONFILE);
        }
    }
}
