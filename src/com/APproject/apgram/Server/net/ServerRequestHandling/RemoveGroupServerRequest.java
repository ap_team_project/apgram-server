package com.APproject.apgram.Server.net.ServerRequestHandlling;

import com.google.gson.Gson;

public class RemoveGroupServerRequest extends ServerRequests implements Runnable{
        private String requestTYPE;
        private String groupID;

        public RemoveGroupServerRequest(String groupID) {
            this.requestTYPE = "RemoveGroupServerRequest";
            this.groupID = groupID;
        }

        public String getRequestTYPE() {
            return requestTYPE;
        }

        public String getGroupID() {
            return groupID;
        }

        @Override
        public void run() {
            String GSONFILE = new Gson().toJson(this);
            //todo connection
        }
}
