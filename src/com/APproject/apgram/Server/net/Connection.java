package com.APproject.apgram.Server.net;

import com.APproject.apgram.Server.Clinet.ClientsManager;
import sbu.ap.jsonParser.json;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Connection implements Runnable, Closeable {
    String id;
    Socket socket;
    InputStream in;
    OutputStream out;
    boolean isConnected =false;
    Lock lock;

    public Connection(Socket socket) {
        this.socket = socket;
        try {
            in = socket.getInputStream();
            out = socket.getOutputStream();
        } catch (IOException e) {}
        new Thread(this).start();
    }

    @Override
    public void run() {
        String useName = null;
        String password = null;
        boolean isCreatingAccount = false;
        json jsonData = json.parser(getJSON());
        try {
            isCreatingAccount = (boolean) jsonData.getDataUsingDot("creatingAccount");
            useName = (String) jsonData.getDataUsingDot("username");
            password = (String) jsonData.getDataUsingDot("password");
        } catch (Exception e) {}

        if(!isCreatingAccount){
            if(ClientsManager.getInstance().signIn(useName,password,this)){
                json response = new json();
                response.put("signedIN",true);
                sendJSON(response.toString());
            }
            else {
                //false information
                json response = new json();
                response.put("signedIN",false);
                sendJSON(response.toString());
                try {
                    close();
                } catch (IOException e) {}
                return;
            }
        }
        else { //creating account
            String id = ClientsManager.getInstance().createAccount(useName,password,this);
            if(id == null){ //cannot create account
                try {
                    json response = new json();
                    response.put("created?",false);
                    sendJSON(response.toString());
                    close();
                } catch (IOException e) {}
                return;
            }
            else { //created account
                json response = new json();
                response.put("created?",true);
                response.put("userID",id);
                sendJSON(response.toString());
            }
        }
        lock = new ReentrantLock();
        isConnected =true;
        new checkConnection();
        while (isConnected){
            String newRequest = getJSON();
            //todo make request handler
        }
    }

    class checkConnection implements Runnable{

        public checkConnection() {
            new Thread(this).start();
        }

        @Override
        public void run() {
            while (socket.isConnected()){
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {}
            }
            isConnected =false;
            ClientsManager.getInstance().userSignOut(id);
        }
    }

    public boolean isConnected() {
        return isConnected;
    }
    public void setID(String id){
        this.id = id;
    }


    public void sendJSON(String json){
        lock.lock();
        ByteBuffer  buffer = ByteBuffer.allocate(1 + 4 + json.length());
        buffer.put((byte) 1);
        buffer.putInt(json.length());
        buffer.put(json.getBytes());
        try {
            out.write(buffer.array());
            out.flush();
        } catch (IOException e) {}
        lock.unlock();
    }
    public String getJSON(){
        byte[] lengthByte = new byte[4];
        try {
            in.read(lengthByte);
            int length = ByteBuffer.wrap(lengthByte).getInt();
            byte[] str = new byte[length];
            in.read(str);
            return new String(str);
        } catch (IOException e) {}
        return null;
    }

    @Override
    public void close() throws IOException {
        socket.close();
        in.close();
        out.close();
    }
}
