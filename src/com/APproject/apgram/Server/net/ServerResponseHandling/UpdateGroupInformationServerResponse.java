package com.APproject.apgram.Server.net.ServerResponseHandlling;

import com.company.Clinet.Client;
import com.company.Clinet.ClientsManager;
import com.company.net.Connection;

public class UpdateGroupInformationServerResponse implements Runnable{
    private String requestTYPE;
    private String groupID;
    private String destinationID;

    public UpdateGroupInformationServerResponse() {
    }

    public String getGroupID() {
        return groupID;
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public void process(){
        new Thread().start();
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        if (ClientsManager.getInstance().getClient(getDestinationID()) != null){
            ClientsManager.getInstance().getClient(getDestinationID()).checkChatUpdate(getGroupID());
        }
    }
}
