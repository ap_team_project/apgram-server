package com.APproject.apgram.Server.net.ServerResponseHandlling;

import com.company.Clinet.ClientsManager;
import com.company.Date.Database.DatabaseManager;
import com.company.Date.Message.Message;
import com.company.Date.Message.MessageType;
import com.company.Util.IdMaker;

import java.util.Date;

public class MessageServerResponse implements Runnable{
    private String requestTYPE;
    private String chatID;
    private String text;
    private String destinationID;


    public MessageServerResponse() {
    }

    public String getChatID() {
        return chatID;
    }

    public String getText() {
        return text;
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public void process(){
        new Thread().start();
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    @Override
    public void run() {
        Date date = new Date();
        String ID = IdMaker.getInstance().getNewMessageID(getChatID());
        Message message = new Message(ID,getText(),null,MessageType.TEXT,date);
        DatabaseManager.getInstance().addMessage(getChatID(),message);
        ClientsManager.getInstance().haveNewMessage(getChatID());
    }
}
