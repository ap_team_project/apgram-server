package com.APproject.apgram.Server.net.ServerResponseHandling;


import com.APproject.apgram.Server.Data.Database.DatabaseManager;
import com.APproject.apgram.Server.Files.DownloadFileManager;

public class ChangeGroupIconServerResponse implements Runnable{
    private String RequestTYPE;
    private String chatID;
    private int index;
    public static final long eachStepCount = 10000;
    private int count;
    private  String hexString;
    private String fileAddress;
    private String destinationID;

    public ChangeGroupIconServerResponse() {
    }

    public String getRequestTYPE() {
        return RequestTYPE;
    }

    public String getChatID() {
        return chatID;
    }

    public int getIndex() {
        return index;
    }

    public static long getEachStepCount() {
        return eachStepCount;
    }

    public int getCount() {
        return count;
    }

    public String getHexString() {
        return hexString;
    }

    public String getFileAddress() {
        return fileAddress;
    }

    public void process(){
        new Thread().start();
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        DownloadFileManager.getInstance().saveIcon(getDestinationID(),getIndex(),getEachStepCount(),getCount(),getHexString());
        if (getIndex() == getCount()){
            DatabaseManager.getInstance().addChatIconSeasonID(getChatID());
        }
    }
}
