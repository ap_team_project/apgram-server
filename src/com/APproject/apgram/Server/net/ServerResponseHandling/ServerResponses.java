package com.APproject.apgram.Server.net.ServerResponseHandlling;

import com.APproject.apgram.Server.net.ServerResponseHandling.CreateChannelServerResponse;
import com.APproject.apgram.Server.net.ServerResponseHandling.CreateGroupServerResponse;
import com.google.gson.*;

public class ServerResponses implements Runnable{
    private String GSONFILE;
    private String requestTYPE;
    private String ID;


    private CreateChannelServerResponse createChannelResponse;
    private CreateGroupServerResponse createGroupResponse;
    private com.APproject.apgram.Server.net.ServerResponseHandlling.AddMembersToChannelServerResponse addMembersToChannelResponse;
    private com.APproject.apgram.Server.net.ServerResponseHandlling.AddMembersToGroupServerResponse addMembersToGroupResponse;
    private com.APproject.apgram.Server.net.ServerResponseHandlling.CreateAccountServerResponse createAccountResponse;
    private com.APproject.apgram.Server.net.ServerResponseHandlling.MessageServerResponse messageResponse;
//    private RenameGroupServerResponse renameGroupResponse;
//    private RenameChannelServerResponse renameChannelResponse;
//    private UpdateChannelInformationServerResponse updateChannelInformationResponse;
//    private UpdateGroupInformationServerResponse updateGroupInformationResponse;
    private com.APproject.apgram.Server.net.ServerResponseHandlling.UpdateUserInformationServerResponse updateUserInformationResponse;
    private com.APproject.apgram.Server.net.ServerResponseHandlling.FileServerResponse fileResponse;
    private com.APproject.apgram.Server.net.ServerResponseHandlling.ChangeChannelIconServerResponse changeChannelIconResponse;
    private com.APproject.apgram.Server.net.ServerResponseHandlling.ChangeGroupIconServerResponse changeGroupIconResponse;
    private com.APproject.apgram.Server.net.ServerResponseHandlling.ChangeProfileIconServerResponse changeProfileIconResponse;
    private com.APproject.apgram.Server.net.ServerResponseHandlling.AddContactServerResponse addContactServerResponse;
    private com.APproject.apgram.Server.net.ServerResponseHandlling.RemoveChannelServerResponse removeChannelServerResponse;
    private com.APproject.apgram.Server.net.ServerResponseHandlling.RemoveGroupServerResponse removeGroupServerResponse;

    public ServerResponses(String GSONFILE , String ID) {
        this.GSONFILE = GSONFILE;
        this.ID = ID;
        new Thread().start();
    }

    @Override
    public void run() {
        // todo find responseTYPE
        int beg_index = 17;
        int end_index = GSONFILE.indexOf(",");
        requestTYPE = GSONFILE.substring(beg_index , end_index);
        if (requestTYPE == "CreateChannelRequest"){
            this.createChannelResponse = new Gson().fromJson(GSONFILE, CreateChannelServerResponse.class);
            this.createChannelResponse.setDestinationID(ID);
            this.createChannelResponse.process();;
        }
        else if (requestTYPE == "CreateGroupRequest"){
            this.createGroupResponse = new Gson().fromJson(GSONFILE, CreateGroupServerResponse.class );
            this.createGroupResponse.setDestinationID(ID);
            this.createGroupResponse.process();
        }
        else if (requestTYPE == "AddMembersToChannelRequest"){
            this.addMembersToChannelResponse = new Gson().fromJson(GSONFILE, AddMembersToChannelServerResponse.class);
            this.addMembersToChannelResponse.setDestinationID(ID);
            this.addMembersToChannelResponse.process();
        }
        else if (requestTYPE == "AddMembersToGroupRequest"){
            this.addMembersToGroupResponse = new Gson().fromJson(GSONFILE, AddMembersToGroupServerResponse.class);
            this.addMembersToGroupResponse.setDestinationID(ID);
            this.addMembersToGroupResponse.process();
        }
        else if (requestTYPE == "CreateAccountRequest"){
            this.createAccountResponse = new Gson().fromJson(GSONFILE, CreateAccountServerResponse.class);
            this.createAccountResponse.setDestinationID(ID);
            this.createAccountResponse.process();
        }
        else if (requestTYPE == "MessageRequest"){
            this.messageResponse = new Gson().fromJson(GSONFILE,MessageServerResponse.class );
            this.messageResponse.setDestinationID(ID);
            this.messageResponse.process();
        }
        else if (requestTYPE == "RenameGroupRequest"){
            this.renameGroupResponse = new Gson().fromJson(GSONFILE,RenameGroupServerResponse.class );
            this.renameGroupResponse.setDestinationID(ID);
            this.renameGroupResponse.process();
        }
        else if (requestTYPE == "RenameChannelRequest"){
            this.renameChannelResponse = new Gson().fromJson(GSONFILE,RenameChannelServerResponse.class );
            this.renameChannelResponse.setDestinationID(ID);
            this.renameChannelResponse.process();
        }
        else if (requestTYPE == "UpdateChannelInformationRequest"){
            this.updateChannelInformationResponse = new Gson().fromJson(GSONFILE, UpdateChannelInformationServerResponse.class);
            this.updateChannelInformationResponse.setDestinationID(ID);
            this.updateChannelInformationResponse.process();
        }
        else if (requestTYPE == "UpdateGroupInformationRequest"){
            this.updateGroupInformationResponse = new Gson().fromJson(GSONFILE, UpdateGroupInformationServerResponse.class);
            this.updateGroupInformationResponse.setDestinationID(ID);
            this.updateGroupInformationResponse.process();
        }
        else if (requestTYPE == "UpdateUserInformationRequest"){
            this.updateUserInformationResponse = new Gson().fromJson(GSONFILE, UpdateUserInformationServerResponse.class);
            this.updateUserInformationResponse.setDestinationID(ID);
            this.updateUserInformationResponse.process();
        }
        else if (requestTYPE == "FileRequest"){
            this.fileResponse = new Gson().fromJson(GSONFILE, FileServerResponse.class);
            this.fileResponse.setDestinationID(ID);
            this.fileResponse.process();
        }
        else if (requestTYPE == "ChangeChannelIconRequest"){
            this.changeChannelIconResponse = new Gson().fromJson(GSONFILE, ChangeChannelIconServerResponse.class);
            this.changeChannelIconResponse.setDestinationID(ID);
            this.changeChannelIconResponse.process();
        }
        else if (requestTYPE == "ChangeGroupIconRequest"){
            this.changeGroupIconResponse = new Gson().fromJson(GSONFILE, ChangeGroupIconServerResponse.class);
            this.changeGroupIconResponse.setDestinationID(ID);
            this.changeGroupIconResponse.process();
        }
        else if(requestTYPE == "ChangeProfileIconRequest"){
            this.changeProfileIconResponse = new Gson().fromJson(GSONFILE , ChangeProfileIconServerResponse.class);
            this.changeProfileIconResponse.setDestinationID(ID);
            this.changeProfileIconResponse.process();
        }
        else if(requestTYPE == "AddContactRequest"){
            this.addContactServerResponse = new Gson().fromJson(GSONFILE,AddContactServerResponse.class);
            this.addContactServerResponse.setDestinationID(ID);
            this.addContactServerResponse.process();
        }
        else if(requestTYPE == "RemoveGroupRequest"){
            this.removeGroupServerResponse = new Gson().fromJson(GSONFILE,RemoveGroupServerResponse.class);
            this.removeGroupServerResponse.setDestinationID(ID);
            this.removeGroupServerResponse.process();
        }
        else{
            this.removeChannelServerResponse = new Gson().fromJson(GSONFILE,RemoveChannelServerResponse.class);
            this.removeChannelServerResponse.setDestinationID(ID);
            this.removeChannelServerResponse.process();
        }

    }
}
