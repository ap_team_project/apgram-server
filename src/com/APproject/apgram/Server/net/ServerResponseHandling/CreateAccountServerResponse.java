package com.APproject.apgram.Server.net.ServerResponseHandlling;

public class CreateAccountServerResponse implements Runnable{
    private String requestTYPE;
    private String USERNAME;
    private String PASSWORD;
    private String destinationID;

    public CreateAccountServerResponse() {
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public void process(){
        new Thread().start();
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    @Override
    public void run() {

    }
}
