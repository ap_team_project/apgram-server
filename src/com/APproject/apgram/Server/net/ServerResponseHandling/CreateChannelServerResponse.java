package com.APproject.apgram.Server.net.ServerResponseHandling;


import java.util.ArrayList;

public class CreateChannelServerResponse implements Runnable{
    private String requestTYPE;
    private String channelName;
    private ArrayList<String> membersUSERNAME;
    private String destinationID;

    public CreateChannelServerResponse() {
    }

    public String getChannelName() {
        return channelName;
    }

    public String getAdminID() {
        return adminID;
    }

    public ArrayList<String> getMembersUSERNAME() {
        return membersUSERNAME;
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public void process(){
        new Thread().start();
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        //todo call addchat
        String ID = IdMaker.getInstance().getChannelID();
        Member member = new Member(getDestinationID(), Access.OWNER);
        DatabaseManager.getInstance().addMember(getDestinationID(),member);
        for (int i = 0 ; i < getMembersUSERNAME().size() ; i++){
            String userID = DatabaseManager.getInstance().getUserIdFromUsername(getMembersUSERNAME().get(i));
            Member newMember = new Member(userID , Access.OWNER);
            DatabaseManager.getInstance().addMember(ID,newMember);
            ClientsManager.getInstance().addMemberToChat(getDestinationID() , newMember.getUserID());
        }
    }
}
