package com.APproject.apgram.Server.net.ServerResponseHandling;


import com.APproject.apgram.Server.Clinet.ClientsManager;
import com.APproject.apgram.Server.Data.Database.DatabaseManager;
import com.APproject.apgram.Server.Data.Messages.Message;
import com.APproject.apgram.Server.Data.Messages.MessageType;
import com.APproject.apgram.Server.Files.DownloadFileManager;
import com.APproject.apgram.Server.Util.IdMaker;
import com.APproject.apgram.Server.Util.LocalIdManager;

import java.io.FileNotFoundException;
import java.util.Date;

public class FileServerResponse implements Runnable{
    private String requestTYPE;
    private String chatID;
    private String localID;
    private String fileTYPE;
    private int index;
    public static final long eachStepCount = 10000;
    private int count;
    private  String hexString;
    private String fileAddress;
    private String destinationID;

    public FileServerResponse() {
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public String getChatID() {
        return chatID;
    }

    public String getLocalID() {
        return localID;
    }

    public String getFileTYPE() {
        return fileTYPE;
    }

    public int getIndex() {
        return index;
    }

    public static long getEachStepCount() {
        return eachStepCount;
    }

    public int getCount() {
        return count;
    }

    public String getHexString() {
        return hexString;
    }

    public String getFileAddress() {
        return fileAddress;
    }

    public void process(){
        new Thread().start();
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        //todo change localID to ID
        if (getIndex() == 1) {
            String ID = IdMaker.getInstance().getNewMessageID(getChatID());
            LocalIdManager.getInstance().setIdWithLocalId(getDestinationID(),getLocalID(),ID);
            DownloadFileManager.getInstance().saveFileMessage(ID,getChatID(),getIndex(),getEachStepCount(),getCount(),getHexString());
        }
        else if (getIndex() < getCount()){
            String ID = LocalIdManager.getInstance().getIdFromLocalId(getDestinationID(),getLocalID());
            DownloadFileManager.getInstance().saveFileMessage(ID,getChatID(),getIndex(),getEachStepCount(),getCount(),getHexString());
        }
        else
        {
            String ID = LocalIdManager.getInstance().getIdFromLocalId(getDestinationID(),getLocalID());
            DownloadFileManager.getInstance().saveFileMessage(ID,getChatID(),getIndex(),getEachStepCount(),getCount(),getHexString());
            LocalIdManager.getInstance().removeLocalId(getDestinationID(),getLocalID());
            Date date = new Date();
            if (getFileTYPE() == "VIDEO"){
                Message message = new Message(ID,null, MessageType.VIDEO,date,false);
                DatabaseManager.getInstance().addMessage(getChatID(),message);
                ClientsManager.getInstance().haveNewMessage(getChatID());
            }
            else if (getFileTYPE() == "MUSIC"){
                Message message = new Message(ID,null, MessageType.MUSIC,date,false);
                DatabaseManager.getInstance().addMessage(getChatID(),message);
                ClientsManager.getInstance().haveNewMessage(getChatID());
            }
            else if (getFileTYPE() == "IMAGE")
            {
                Message message = new Message(ID,null, MessageType.IMAGE,date,false);
                DatabaseManager.getInstance().addMessage(getChatID(),message);
                ClientsManager.getInstance().haveNewMessage(getChatID());
            }
            else {
                Message message = new Message(ID,null, MessageType.FILE,date,false);
                DatabaseManager.getInstance().addMessage(getChatID(),message);
                ClientsManager.getInstance().haveNewMessage(getChatID());
            }

        }

    }
}
