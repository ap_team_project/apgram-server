package com.APproject.apgram.Server.net.ServerResponseHandlling;

import com.company.Clinet.Client;
import com.company.Clinet.ClientsManager;
import com.company.net.Connection;

public class UpdateChannelInformationServerResponse implements Runnable{
    private String requestTYPE;
    private String channelID;
    private String destinationID;

    public UpdateChannelInformationServerResponse() {
    }

    public String getChannelID() {
        return channelID;
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public void process(){
        new Thread().start();
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        if (ClientsManager.getInstance().getClient(getDestinationID()) != null) {
            ClientsManager.getInstance().getClient(getDestinationID()).checkChatUpdate(getChannelID());
        }
    }
}
