package com.APproject.apgram.Server.net.ServerResponseHandlling;

import com.company.Clinet.Client;
import com.company.Clinet.ClientsManager;
import com.company.Date.Database.DatabaseManager;
import com.company.net.Connection;

public class UpdateUserInformationServerResponse implements Runnable{
    private String requestTYPE;
    private String userName;
    private String ID;
    private boolean isOnline;
    private String lastSeen;
    private String destinationID;

    public UpdateUserInformationServerResponse() {
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public String getUserName() {
        return userName;
    }

    public String getID() {
        return ID;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void process(){
        new Thread().start();
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        String userID = DatabaseManager.getInstance().getUserIdFromUsername(getUserName());
        if (ClientsManager.getInstance().getClient(userID) != null){
            ClientsManager.getInstance().getClient(userID).checkUserUpdate(userID);
        }
    }
}
