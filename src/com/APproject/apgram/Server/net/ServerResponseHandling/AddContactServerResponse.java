package com.APproject.apgram.Server.net.ServerResponseHandlling;

import com.company.Clinet.ClientsManager;
import com.company.Date.Database.DatabaseManager;
import com.google.gson.Gson;

public class AddContactServerResponse implements Runnable{
    private String requestTYPE;
    private String USERNAME;
    private String destinationID;

    public AddContactServerResponse() {
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public void process(){
        new Thread().start();
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        if (ClientsManager.getInstance().getClient(getDestinationID()) != null){
            ClientsManager.getInstance().getClient().addContact();
        }
    }
}
