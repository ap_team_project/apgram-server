package com.APproject.apgram.Server.net.ServerResponseHandlling;

import com.company.Clinet.ClientsManager;
import com.company.Date.Chat.Access;
import com.company.Date.Chat.Member;
import com.company.Date.Database.DatabaseManager;

import java.util.ArrayList;

public class AddMembersToGroupServerResponse implements Runnable{
    private String requestTYPE;
    private String chatID;
    private ArrayList<String> membersUSERNAME;
    private String destinationID;

    public AddMembersToGroupServerResponse() {
    }

    public String getChatID() {
        return chatID;
    }

    public ArrayList<String> getMembersUSERNAME() {
        return membersUSERNAME;
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public void process(){
        new Thread().start();
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        for (int i = 0 ; i < getMembersUSERNAME().size() ; i++){
            String userID = DatabaseManager.getInstance().getUserIdFromUsername((String) getMembersUSERNAME().get(i));
            Member newMember = new Member(userID , Access.NORMAL);
            DatabaseManager.getInstance().addMember(getChatID(),newMember);
            ClientsManager.getInstance().addMemberToChat(chatID , newMember.getUserID());
        }
    }
}
