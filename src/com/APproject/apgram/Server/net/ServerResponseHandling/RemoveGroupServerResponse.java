package com.APproject.apgram.Server.net.ServerResponseHandlling;

import com.company.Clinet.ClientsManager;
import com.company.Date.Chat.Chat;
import com.company.Date.Chat.ChatType;
import com.company.Date.Database.DatabaseManager;
import com.company.net.Connection;

public class RenameGroupServerResponse implements Runnable{
    private String requestTYPE;
    private String groupID;
    private String groupNewNAME;
    private String destinationID;

    public RenameGroupServerResponse() {
    }

    public String getGroupID() {
        return groupID;
    }

    public String getGroupNewNAME() {
        return groupNewNAME;
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public void process(){
        new Thread().start();
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        Chat chat = new Chat(getGroupID(), ChatType.CHANNEL,getGroupNewNAME(),false,0,0);
        if (ClientsManager.getInstance().getClient(getDestinationID()) != null){
            DatabaseManager.getInstance().updateChat(chat);
        }
    }
}
