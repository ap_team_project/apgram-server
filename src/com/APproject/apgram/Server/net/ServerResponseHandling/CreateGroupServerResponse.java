package com.APproject.apgram.Server.net.ServerResponseHandling;

import com.APproject.apgram.Server.Clinet.ClientsManager;
import com.APproject.apgram.Server.Data.Chats.Access;
import com.APproject.apgram.Server.Data.Chats.Member;
import com.APproject.apgram.Server.Data.Database.DatabaseManager;
import com.APproject.apgram.Server.Util.IdMaker;

import java.util.ArrayList;

public class CreateGroupServerResponse implements Runnable{
    private String requestTYPE;
    private String groupNAME;
    private String adminID;
    private ArrayList<String> membersUSERNAME;
    private String destinationID;

    public CreateGroupServerResponse() {
    }

    public String getGroupNAME() {
        return groupNAME;
    }

    public String getAdminID() {
        return adminID;
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public ArrayList<String> getMembersUSERNAME() {
        return membersUSERNAME;
    }

    public void process(){
        new Thread().start();
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        //todo call addchat
        String ID = IdMaker.getInstance().getGroupID();
        Member member = new Member(getDestinationID(), Access.OWNER);
        DatabaseManager.getInstance().addMember(getDestinationID(),member);
        for (int i = 0 ; i < getMembersUSERNAME().size() ; i++){
            String userID = DatabaseManager.getInstance().getUserIdFromUsername(getMembersUSERNAME().get(i));
            Member newMember = new Member(userID , Access.OWNER);
            DatabaseManager.getInstance().addMember(ID,newMember);
            ClientsManager.getInstance().addMemberToChat(getDestinationID(),newMember.getUserID());
        }
    }
}
