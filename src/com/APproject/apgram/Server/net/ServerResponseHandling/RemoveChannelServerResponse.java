package com.APproject.apgram.Server.net.ServerResponseHandlling;

import com.company.Clinet.ClientsManager;
import com.company.Date.Chat.Chat;
import com.company.Date.Chat.ChatType;
import com.company.Date.Database.DatabaseManager;
import com.company.net.Connection;

public class RenameChannelServerResponse implements Runnable{
    private String requestTYPE;
    private String channelID;
    private String channelNewNAME;
    private String destinationID;

    public RenameChannelServerResponse() {
    }

    public String getChannelID() {
        return channelID;
    }

    public String getChannelNewNAME() {
        return channelNewNAME;
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    public void process(){
        new Thread().start();
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        Chat chat = new Chat(getChannelID(), ChatType.CHANNEL,getChannelNewNAME(),false,0,0);
        if (ClientsManager.getInstance().getClient(getDestinationID()) != null){
            DatabaseManager.getInstance().updateChat(chat);
        }
    }
}
