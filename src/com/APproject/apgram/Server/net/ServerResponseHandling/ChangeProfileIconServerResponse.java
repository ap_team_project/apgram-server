package com.APproject.apgram.Server.net.ServerResponseHandlling;

import com.company.Date.Database.DatabaseManager;
import com.company.File.DownloadFileManager;
import com.company.Util.IdMaker;
import com.company.Util.LocalIdManager;

public class ChangeProfileIconServerResponse implements Runnable{
    private String RequestTYPE;
    private String userID;
    private int index;
    public static final long eachStepCount = 10000;
    private int count;
    private  String hexString;
    private String fileAddress;
    private String destinationID;


    public ChangeProfileIconServerResponse() {
    }

    public String getUserID() {
        return userID;
    }

    public String getRequestTYPE() {
        return RequestTYPE;
    }

    public int getIndex() {
        return index;
    }

    public static long getEachStepCount() {
        return eachStepCount;
    }

    public int getCount() {
        return count;
    }

    public String getHexString() {
        return hexString;
    }

    public String getFileAddress() {
        return fileAddress;
    }

    public void process(){
        new Thread().start();
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        DownloadFileManager.getInstance().saveIcon(getDestinationID(),getIndex(),getEachStepCount(),getCount(),getHexString());
        if (getCount() == getIndex()){
            DatabaseManager.getInstance().addIconSeasonID(getUserID());
        }
    }
}
