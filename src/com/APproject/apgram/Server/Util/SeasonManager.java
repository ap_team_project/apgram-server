package com.APproject.apgram.Server.Util;

import com.APproject.apgram.Server.Files.FileManager;

import java.io.Closeable;
import java.io.Serializable;
import java.util.HashMap;

public class SeasonManager implements Serializable, Closeable {
    String id;
    HashMap<String,Integer> chatAndUserData;
    HashMap<String, String> messageSeason;

    public SeasonManager(String id){
        this.id = id;
        chatAndUserData = new HashMap<>();
        messageSeason = new HashMap<>();
    }

    public String getID() {
        return id;
    }

    public int getSeason(String id){
        return chatAndUserData.getOrDefault(id,0);
    }
    public void setSeason(String id, int seasonID){
        chatAndUserData.put(id,seasonID);
    }

    public int getIconSeason(String id){
        return chatAndUserData.getOrDefault("i"+id,0);
    }
    public void setIconSeason(String id, int seasonId){
        chatAndUserData.put("i"+id,seasonId);
    }

    public String getMessageSeason(String chatID){
        if(messageSeason.containsKey(chatID)){
            return messageSeason.get(chatID);
        }
        return null;
    }
    public void setMessageSeason(String chatID, String messageID){
        messageSeason.put(chatID,messageID);
    }



    @Override
    public void close(){
        FileManager.getInstance().saveSeasonManager(this);
    }
}
