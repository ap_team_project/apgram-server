package com.APproject.apgram.Server.Util;

import com.APproject.apgram.Server.Files.FileManager;

import java.io.Closeable;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;

public class LocalIdManager implements Serializable, Closeable {
    static LocalIdManager singleLocalIdManager;

    public static LocalIdManager getInstance() {
        return singleLocalIdManager;
    }

    HashMap<String,HashMap<String ,String>> data = new HashMap<>();

    public void start(){
        singleLocalIdManager = this;
    }


    public String getIdFromLocalId(String userID, String localID){
        HashMap<String ,String> userHashMap = data.getOrDefault(userID, null);
        return userHashMap==null?null:userHashMap.getOrDefault(localID,null);
    }

    public void setIdWithLocalId(String userID, String localID, String id){
        if(!data.containsKey(userID)){
            data.put(userID, new HashMap<>());
        }
        data.get(userID).put(localID,id);
    }
    public void removeLocalId(String userID, String localID){
        if (!data.containsKey(userID)) {
            return;
        }
        data.get(userID).remove(localID);
    }


    @Override
    public void close(){
        FileManager.getInstance().saveLocalIdManager();
    }
}
