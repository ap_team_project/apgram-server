package com.APproject.apgram.Server.Util;

import com.APproject.apgram.Server.Files.FileManager;

import java.io.Closeable;
import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class IdMaker implements Serializable , Closeable {
    private static IdMaker singleIdMaker = null;

    public static IdMaker getInstance(){
        return singleIdMaker;
    }

    HashMap<String,Long> IDs;
    HashMap<String , Integer> messageID;
    Lock lock;

    public IdMaker(){
        IDs= new HashMap<>();
        messageID = new HashMap<>();
        IDs.put("u", (long) 0);
        IDs.put("g", (long) 0);
        IDs.put("c", (long) 0);
        lock = new ReentrantLock();
    }

    public void start(){
        singleIdMaker = this;
    }

    public String getUserID(){
        lock.lock();
        long l = IDs.get("u");
        IDs.put("u",l+1);
        lock.unlock();
        return "u"+l;
    }
    public String getGroupID(){
        lock.lock();
        long l = IDs.get("g");
        IDs.put("u",l+1);
        lock.unlock();
        return "u"+l;
    }
    public String getChannelID(){
        lock.lock();
        long l = IDs.get("c");
        IDs.put("u",l+1);
        lock.unlock();
        return "u"+l;
    }


    public String getNewMessageID(String chatID){
        lock.lock();
        String result = chatID;
        if(messageID.containsKey(chatID)){
            int position = messageID.get(chatID);
            result = result+"#"+position;
            messageID.put(chatID,position+1);
        }
        else {
            messageID.put(chatID,2);
            result = result + "#1";
        }
        lock.unlock();
        return result;
    }

    @Override
    public void close(){
        FileManager.getInstance().saveIdMaker();
    }
}
